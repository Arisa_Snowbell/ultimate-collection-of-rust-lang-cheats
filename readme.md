1. rustup docs --book | https://doc.rust-lang.org/book/ | https://doc.rust-lang.org/stable/rust-by-example/
2. /Memory Container Cheat-sheet | https://github.com/usagi/rust-memory-container-cs
3. /Patterns | https://github.com/rust-unofficial/patterns
4. /cheats.rs | https://cheats.rs/
5. /async-book |  https://rust-lang.github.io/async-book/
6. /Rustlings | https://github.com/rust-lang/rustlings
7. /The Standard Library | https://doc.rust-lang.org/std/index.html
8. /Edition Guide | https://doc.rust-lang.org/edition-guide/index.html
9. /Cargo Book | https://doc.rust-lang.org/cargo/index.html
10. /RustDoc Book | https://doc.rust-lang.org/rustdoc/index.html
11. /RustC Book | https://doc.rust-lang.org/rustc/index.html
12. /Compiler Error Index | https://doc.rust-lang.org/error-index.html
13. /Command Line Book | https://rust-cli.github.io/book/index.html
14. /WebAssembly Book | https://rustwasm.github.io/docs/book/
15. /Embedded Book | https://doc.rust-lang.org/embedded-book
16. /Reference Book | https://doc.rust-lang.org/reference/index.html
17. /Unstable Rust 'nomicon | https://doc.rust-lang.org/nomicon/index.html
18. /Unstable Book | https://doc.rust-lang.org/nightly/unstable-book/index.html
19. /Design Patterns Book | https://rust-unofficial.github.io/patterns/
20. /Easy Book | https://dhghomon.github.io/easy_rust/
21. /Learning Rust | https://learning-rust.github.io/
22. /Rust Quiz | https://dtolnay.github.io/rust-quiz/
